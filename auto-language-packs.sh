#!/bin/bash
#Script to install language packs:
echo This script will try to perform an update first. Make sure you are on-line!
#Perform system update
sudo apt update
#Detect system language
lang=$(locale | grep LANG= | cut -d= -f2 | cut -d. -f1)
#corrected_lang=
OUTPUT="${lang//[\_\-\`]}"
corrected_lang_sign=$(echo "${lang//_/-}")
corrected_lang_lowercase=$(echo "${corrected_lang_sign,,}")

#Not tested- there seems to be only "fr" language packs, so probably we'll have to try to convert variable with "fr-*" to "fr" -If this works, adapt to other languages, as needed:
if [[ $corrected_lang_lowercase == *"fr"* ]]; then
  corrected_lang_lowercase=fr
fi

#check if firefox-esr is installed, if so, try to localize it
if  [ -x "$(command -v firefox-esr)" ]; then
#state packege name to install, without the language code:
partial_package_name=firefox-esr-l10n-
#add the correct language code to the end of the package name
package_name=$partial_package_name$corrected_lang_lowercase
   echo firefox-esr is installed... Trying to install localization package...
#try to install the localization package
   sudo apt install $package_name
fi

#check if libreoffice is installed, if so, try to localize it
#try to correct language code problems: there is no libroffice pt-pt language pack, only "pt" or "pt-br" so, convert language variable from "pt-pt" to "pt", if needed.
if [[ $corrected_lang_lowercase == *"pt-pt"* ]]; then
  corrected_lang_lowercase=pt
fi

#Check if libreoffice is installed, if so, try to localize it...
if  [ -x "$(command -v libreoffice)" ]; then
#state packege name to install, without the language code:
partial_package_name=libreoffice-l10n-
#add the correct language code to the end of the package name
package_name=$partial_package_name$corrected_lang_lowercase
   echo Libreoffice is installed... Trying to install localization package...
#try to install the localization package
   sudo apt install $package_name
fi
echo DONE! You can now close this window!
