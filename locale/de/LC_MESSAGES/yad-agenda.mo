��    
      l      �       �   @   �   U   2     �     �     �     �     �     �     �  �  �  O   y  ~   �     H     X     a     h     o     �     �         
               	                      Incorrect time entered \n Enter a time between 00:00 and 23:59  Double click an event to edit or cancel it, double click date to add an all day event End (optional) Event OK Start \n  $event_description\n antiX Calendar antiX Calendar Alarm Project-Id-Version: contribs-goodies
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-06-01 11:01+0000
Last-Translator: Not Telling <j.xecure@gmail.com>, 2021
Language-Team: German (https://www.transifex.com/antix-linux-community-contributions/teams/120110/de/)
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Ungültige Zeitangabe. \n Gültige Zeitangaben liegen zwischen 00:00 und 23:59  Doppelklick auf einen Eintrag zum Bearbeiten oder Austragen; Doppelklick auf ein Datum fügt einen ganztägigen Eintrag hinzu. Ende (optional) Ereignis Weiter Beginn \n  $event_description\n antiX Kalender antiX Kalender Erinnerung 