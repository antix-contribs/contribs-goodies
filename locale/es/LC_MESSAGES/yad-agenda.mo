��    
      l      �       �   @   �   U   2     �     �     �     �     �     �     �  �  �  D   z  �   �     J     Y     `     h     q     �     �         
               	                      Incorrect time entered \n Enter a time between 00:00 and 23:59  Double click an event to edit or cancel it, double click date to add an all day event End (optional) Event OK Start \n  $event_description\n antiX Calendar antiX Calendar Alarm Project-Id-Version: contribs-goodies
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-06-01 11:01+0000
Last-Translator: Not Telling <j.xecure@gmail.com>, 2021
Language-Team: Spanish (https://www.transifex.com/antix-linux-community-contributions/teams/120110/es/)
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
  Hora incorrecta \n Introduzca una hora entre las 00:00 y las 23:59  Pulse (doble click) sobre un evento para editarlo o cancelarlo. Pulse (double click) una fecha para crear un evento que dura todo el día. Fin (opcional) Evento Aceptar Comienzo \n  $event_description\n Calendario para antiX Calendario y Alarma para antiX 