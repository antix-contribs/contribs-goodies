# contribs-goodies

Collection of small scripts from the antiX community that gives small improvements to the antiX Linux experience.

->**access-cloud**- it's a script that acts as a front end to mount open cloud drives (like google drive, one drive, etc) making them accessible on your default file manager, just like a local drive.
 In it's configuration menu, it includes a dedicated button to automatically configure "Google Drive", all other cloud drives have to be configured using a terminal interface. Once the initial configuration of the cloud drive is done, the script is intirely Graphics User Interface- click the cloud drive you want to access on the main window, and it opens in your file manager (or you can also unmount all mounted cloud drives or configure cloud drives)

->**debinstaller**- it's a front end to the "sudo apt install" and "sudo apt purge" commands. This script is meant to be associated with the .deb MIME file type, and launche when the user clicks a .deb package in a file manager.
 It allows the user to install the package or, if the same package (in whatever version) is already installed, it also allows to uninstall (purge) the package from the system.
 It works very much like antix-installer- it's a GUI that fires up the terminal, so the user can know exactly what changes are being done, and/or provide input, if needed- contact with the terminal is kept to a minimum, the user does not have to type any command.

 ->**yad-agenda**- it's a yad script that improves yad calendar's functionality turning it into a **basic** GUI agenda- you can click any day from the calendar and add all day events or, if you set a start time, an alarm will sound at the set date, and a pop up window will display the event. It can also search for events.
  It can be used as replacement for the yad-calendar that currently antiX linux uses to the display the calendar- it looks almost the same, but days with events are shown in bold, and it also features the "search" icon, next to the "close" icon.
   All events are stored in a plain text file called yad-calendar.txt, in your home folder. That file can be manually edited (one day per line)
   Please note: this is a "simple" agenda, it does not offer recurring events, nor the possibily to set alarms before the time of an event. It's really like using a paper agenda- but it has alarms and allows you to quicky search trough all of it's contents...
   When the calendar reminders are running, the agenda uses about 1 MB of RAM (not too bad).
